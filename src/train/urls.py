from django.urls import path
from train import views

urlpatterns = [
    path('sgd/', views.getTestResultSGD),
    path('als/', views.getTestResultALS),
    path('methods/', views.getMethods),
]
