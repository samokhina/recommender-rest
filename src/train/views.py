from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from .serializers import MethodSerializer, TestSerializer
from .models import Test, Method


# http://127.0.0.1:8000/api/experiment/sgd/
@api_view(['GET'])
def getTestResultSGD(request):
    test = Test.objects.filter(method__name="sgd")
    serializer = TestSerializer(test, many=True)
    return JsonResponse({"body": serializer.data[0]})


# http://127.0.0.1:8000/api/experiment/als/
@api_view(['GET'])
def getTestResultALS(request):
    test = Test.objects.filter(method__name="als")
    serializer = TestSerializer(test, many=True)
    return JsonResponse({"body": serializer.data[0]})


# http://127.0.0.1:8000/api/experiment/methods
@api_view(['GET'])
def getMethods(request):
    methods = Method.objects.all()
    serializer = MethodSerializer(methods, many=True)
    return JsonResponse({"methods": serializer.data})

