from rest_framework import serializers

import sys
sys.path.append("..")
from recommender.serializers import TrainSerializer

class ReviewSerializer(serializers.Serializer):
    userId = serializers.IntegerField()
    bookId = serializers.IntegerField()
    reviewId = serializers.IntegerField()
    rating = serializers.FloatField()


class ErrorSerializer(serializers.Serializer):
    errorTest = serializers.FloatField()
    errorTrain = serializers.FloatField()
    iteration = serializers.IntegerField()


class TestFactorSerializer(serializers.Serializer):
    factor = serializers.IntegerField()
    errors = ErrorSerializer(many=True, read_only=True)


class TestSerializer(serializers.Serializer):
    regularization = serializers.FloatField()
    rate = serializers.FloatField()
    maxIterations = serializers.IntegerField()
    dividingSample = serializers.CharField(max_length=100)
    factors = TestFactorSerializer(many=True, read_only=True)


class MethodSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    tests = TestSerializer(many=True, read_only=True)
    trains = TrainSerializer(many=True, read_only=True)

