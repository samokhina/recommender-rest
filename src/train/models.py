from django.db import models


class Review(models.Model):
    userId = models.IntegerField()
    bookId = models.IntegerField()
    reviewId = models.IntegerField()
    rating = models.FloatField()


class Method(models.Model):
    name = models.CharField(max_length=20)


class Test(models.Model):
    regularization = models.FloatField()
    rate = models.FloatField()
    maxIterations = models.IntegerField()
    dividingSample = models.CharField(max_length=100)
    method = models.ForeignKey(Method, related_name='tests', on_delete=models.CASCADE)


class TestFactor(models.Model):
    factor = models.IntegerField(default=0)
    test = models.ForeignKey(Test, related_name='factors', on_delete=models.CASCADE)


class Error(models.Model):
    errorTest = models.FloatField()
    errorTrain = models.FloatField()
    iteration = models.IntegerField(default=0)
    factor = models.ForeignKey(TestFactor, related_name='errors', on_delete=models.CASCADE)
