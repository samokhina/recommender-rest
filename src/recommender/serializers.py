from rest_framework import serializers


class ItemBiasSerializer(serializers.Serializer):
    factor = serializers.IntegerField()
    value = serializers.FloatField()
    itemId= serializers.IntegerField()


class UserBiasSerializer(serializers.Serializer):
    factor = serializers.IntegerField()
    value = serializers.FloatField()
    userId = serializers.IntegerField()


class ItemFactorSerializer(serializers.Serializer):
    factor = serializers.IntegerField()
    value = serializers.FloatField()


class ItemFactorsSerializer(serializers.Serializer):
    itemId = serializers.IntegerField()
    factors = ItemFactorSerializer(many=True, read_only=True)


class UserFactorSerializer(serializers.Serializer):
    factor = serializers.IntegerField()
    value = serializers.FloatField()


class UserFactorsSerializer(serializers.Serializer):
    userId = serializers.IntegerField()
    factors = UserFactorSerializer(many=True, read_only=True)


class TrainSerializer(serializers.Serializer):
    regularization = serializers.FloatField()
    rate = serializers.FloatField()
    maxIterations = serializers.IntegerField()
    factorsCount = serializers.IntegerField()
    ratingMean = serializers.FloatField()
    usersFactors = UserFactorsSerializer(many=True, read_only=True)
    itemsFactors = ItemFactorsSerializer(many=True, read_only=True)
    usersBias = UserBiasSerializer(many=True, read_only=True)
    itemsBias = ItemBiasSerializer(many=True, read_only=True)
