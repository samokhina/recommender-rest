from django.urls import path
from recommender import views

urlpatterns = [
    path('sgd/user/<int:userId>/', views.getRecommendationsByUserIdSGD),
    path('als/user/<int:userId>/', views.getRecommendationsByUserIdALS),
]
