from django.db import models
import sys
sys.path.append("..")
from train.models import Method


class Train(models.Model):
    regularization = models.FloatField()
    rate = models.FloatField()
    maxIterations = models.IntegerField()
    factorsCount = models.IntegerField()
    ratingMean = models.FloatField()
    method = models.ForeignKey(Method, related_name='trains', on_delete=models.CASCADE)


class UserFactors(models.Model):
    userId = models.IntegerField()
    train = models.ForeignKey(Train, related_name='usersFactors', on_delete=models.CASCADE)


class UserFactor(models.Model):
    factor = models.IntegerField()
    value = models.FloatField()
    userFactors = models.ForeignKey(UserFactors, related_name='factors', on_delete=models.CASCADE)


class ItemFactors(models.Model):
    itemId = models.IntegerField()
    train = models.ForeignKey(Train, related_name='itemsFactors', on_delete=models.CASCADE)


class ItemFactor(models.Model):
    factor = models.IntegerField()
    value = models.FloatField()
    itemFactors = models.ForeignKey(ItemFactors, related_name='factors', on_delete=models.CASCADE)


class UserBias(models.Model):
    userId = models.IntegerField()
    factor = models.IntegerField(default=0)
    value = models.FloatField(default=0)
    train = models.ForeignKey(Train, related_name='usersBias', on_delete=models.CASCADE)


class ItemBias(models.Model):
    itemId = models.IntegerField()
    factor = models.IntegerField(default=0)
    value = models.FloatField(default=0)
    train = models.ForeignKey(Train, related_name='itemsBias', on_delete=models.CASCADE)