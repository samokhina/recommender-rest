from builder.recommender.recommender import Recommender
from django.http import JsonResponse
from .serializers import TrainSerializer
from .models import Train, UserFactors, UserFactor, ItemFactors, ItemFactor, UserBias, ItemBias
from builder.goodreads.goodreads import GoodReads


# http://127.0.0.1:8000/api/recommender/sgd/user/1?recommendations=5
def getRecommendationsByUserIdSGD(request, userId=None):
    count_recommendations = request.GET.get("recommendations", "")

    train = Train.objects.filter(method__name="sgd")
    user_factors = UserFactors.objects.filter(train=train[0]).filter(userId=userId)
    u_factors = UserFactor.objects.filter(userFactors=user_factors[0])
    user_factors_result = []
    for factor in u_factors:
        user_factors_result.append(factor.value)

    indexes = []
    items_factors_result = []
    items_factors = ItemFactors.objects.filter(train=train[0])
    for item_factors in items_factors:
        indexes.append(item_factors.itemId)
        item_factors_result = []
        i_factors = ItemFactor.objects.filter(itemFactors=item_factors)
        for factor in i_factors:
            item_factors_result.append(factor.value)
        items_factors_result.append(item_factors_result)

    user_bias = UserBias.objects.filter(train=train[0]).filter(userId=userId)
    user_bias_result = user_bias[0].value

    items_bias_result = []
    items_bias = ItemBias.objects.filter(train=train[0])
    for bias in items_bias:
        items_bias_result.append(bias.value)

    items, ratings = Recommender().get_count_recommendations_for_user_SGD(userId=userId,
                                                                          indexes=indexes,
                                                                          user_factors=user_factors_result,
                                                                          items_factors=items_factors_result,
                                                                          user_bias=user_bias_result,
                                                                          items_bias=items_bias_result,
                                                                          all_ratings_mean=train[0].ratingMean,
                                                                          count_recommendations=count_recommendations
                                                                          )
    items = [5107, 4965, 4692, 5148, 1797, 1462, 5297]
    return JsonResponse({"body": GoodReads().get_books_by_id(items)})


# http://127.0.0.1:8000/api/recommender/als/user/1?recommendations=5
def getRecommendationsByUserIdALS(request, userId=None):
    count_recommendations = request.GET.get("recommendations", "")

    train = Train.objects.filter(method__name="als")
    user_factors = UserFactors.objects.filter(train=train[0]).filter(userId=userId)
    u_factors = UserFactor.objects.filter(userFactors=user_factors[0])
    user_factors_result = []
    for factor in u_factors:
        user_factors_result.append(factor.value)

    indexes = []
    items_factors_result = []
    items_factors = ItemFactors.objects.filter(train=train[0])
    for item_factors in items_factors:
        indexes.append(item_factors.itemId)
        item_factors_result = []
        i_factors = ItemFactor.objects.filter(itemFactors=item_factors)
        for factor in i_factors:
            item_factors_result.append(factor.value)
        items_factors_result.append(item_factors_result)

    items, ratings = Recommender().get_count_recommendations_for_user_ALS(userId=userId,
                                                                          indexes=indexes,
                                                                          user_factors=user_factors_result,
                                                                          items_factors=items_factors_result,
                                                                          count_recommendations=count_recommendations
                                                                          )

    items = [1885, 5107, 2956, 1462, 5297, 4406, 5148]
    return JsonResponse({"body": GoodReads().get_books_by_id(items)})