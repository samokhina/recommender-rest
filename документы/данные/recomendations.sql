INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 1, 1, 5.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 2, 2, 3.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 3, 3, 5.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 7, 7, 4.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 8, 8, 3.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 10, 10, 2.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 11, 11, 4.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 12, 12, 4.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 14, 14, 1.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (1, 15, 15, 3.0);

INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 1, 16, 1.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 4, 19, 1.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 6, 21, 4.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 7, 22, 5.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 9, 24, 2.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 11, 26, 3.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 12, 27, 3.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (2, 14, 29, 4.0);

INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 1, 31, 4.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 2, 32, 4.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 3, 33, 5.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 8, 38, 4.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 9, 39, 3.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 10, 40, 5.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (3, 15, 45, 5.0);

INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 1, 46, 2.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 2, 47, 1.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 3, 48, 2.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 4, 49, 1.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 5, 50, 2.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 6, 51, 4.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 7, 52, 4.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 8, 53, 0.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 9, 54, 1.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 10, 55, 1.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 11, 56, 2.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 12, 57, 2.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 13, 58, 3.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 14, 59, 3.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (4, 15, 60, 2.0);

INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (5, 5, 61, 2.0);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (5, 12, 62, 1.5);
INSERT INTO `train_review` (`userId`, `bookId`, `reviewId`, `rating`) VALUES (5, 13, 63, 4.0);




INSERT INTO `train_method` (`name`) VALUES ("sgd");
INSERT INTO `train_method` (`name`) VALUES ("als");

CREATE TABLE review (
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `userId` INT,
  `bookId` INT,
  `reviewId` INT,
  `rating` DOUBLE
);





