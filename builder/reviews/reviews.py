import numpy as np
import pandas as pd
import scipy.sparse as sparse


class Reviews(object):

    def __init__(self, reviews=None):
        self.reviews = reviews

    def transformation_reviews(self):
        reviews_data = self.reviews.drop(self.reviews.columns[0], axis=1)
        reviews_data = reviews_data.drop(reviews_data.columns[2], axis=1)

        data = reviews_data.dropna()
        data = data.loc[data.rating != 0.]

        data['user_id'] = data['userId'].astype("category").cat.codes
        data['book_id'] = data['bookId'].astype("category").cat.codes

        books_ids = data[['book_id', 'bookId']].drop_duplicates()
        books_ids['book_id'] = books_ids.book_id.astype(str)

        users_ids = data[['user_id', 'userId']].drop_duplicates()
        users_ids['user_id'] = users_ids.user_id.astype(str)

        data = data.drop(['userId', 'bookId'], axis=1)

        users_count = len(list(np.sort(data.user_id.unique())))
        books_count = len(list(np.sort(data.book_id.unique())))

        ratings = list(data.rating)
        rows = data.user_id.astype(int)
        cols = data.book_id.astype(int)
        ratings_data = sparse.csr_matrix((ratings, (rows, cols)), shape=(users_count, books_count))

        data = data.loc[:, ['user_id', 'book_id', 'rating']]
        return users_ids, books_ids, ratings_data, data

    def split_reviews(self, percent_test=20):
        reviews_data = self.reviews.drop(self.reviews.columns[0], axis=1)
        reviews_data = reviews_data.drop(reviews_data.columns[2], axis=1)

        data = reviews_data.dropna()
        data = data.loc[data.rating != 0.]

        data['user_id'] = data['userId'].astype("category").cat.codes
        data['book_id'] = data['bookId'].astype("category").cat.codes

        # Словари books_ids и users_ids
        books_ids = data[['book_id', 'bookId']].drop_duplicates()
        books_ids['book_id'] = books_ids.book_id.astype(str)

        users_ids = data[['user_id', 'userId']].drop_duplicates()
        users_ids['user_id'] = users_ids.user_id.astype(str)

        data = data.drop(['userId', 'bookId'], axis=1)

        # ----------------------------------------------------------------------------

        columns = ['user_id', 'book_id', 'rating']
        test = pd.DataFrame(columns=columns)
        train = pd.DataFrame(columns=columns)

        users = set(users_ids.values[:, 0])
        for u in users:
            index = data['user_id'].isin(set(u))
            count = data[index].groupby('user_id').rating.count().values[0]
            count_test = int((count * percent_test / 100))

            data_test = data[index].groupby('user_id').head(count_test)
            test = test.append(data_test)

            data_train = data[index][~data[index].index.isin(data_test.index)]
            train = train.append(data_train)

        # ----------------------------------------------------------------------------

        users_count = len(list(np.sort(data.user_id.unique())))
        books_count = len(list(np.sort(data.book_id.unique())))

        ratings_train = list(train.rating)
        rows_train = train.user_id.astype(int)
        cols_train = train.book_id.astype(int)
        data_train_matrix = sparse.csr_matrix((ratings_train, (rows_train, cols_train)),
                                              shape=(users_count, books_count))
        data_train = train.loc[:, ['user_id', 'book_id', 'rating']]

        ratings_test = list(test.rating)
        rows_test = test.user_id.astype(int)
        cols_test = test.book_id.astype(int)
        data_test_matrix = sparse.csr_matrix((ratings_test, (rows_test, cols_test)),
                                             shape=(users_count, books_count))
        data_test = test.loc[:, ['user_id', 'book_id', 'rating']]

        return data_train_matrix, data_train, data_test_matrix, data_test
