import sys
import math
import numpy as np
import scipy.sparse as sparse
from scipy.sparse.linalg import spsolve


class ALS(object):

    def __init__(self, ratings=None, max_iterations=100, regularization=0.02, rate=0.02):
        self.MAX_ITERATIONS = max_iterations
        self.ratings = ratings
        self.regularization = regularization

        self.lI = None
        self.user_factors = None
        self.item_factors = None
        self.count_factors = None

    def initialize_factors(self, count_factors=25):
        self.count_factors = count_factors
        user_size, item_size = self.ratings.shape # размеры выборок одинаковые
        self.user_factors = sparse.csr_matrix(np.full((user_size, count_factors), 0.95))  # m*k или user_size*features
        self.item_factors = sparse.csr_matrix(np.full((item_size, count_factors), 0.95))  # n*k или item_size*features
        self.lI = self.regularization * sparse.eye(count_factors)

    def train(self, count_factors=2):
        self.initialize_factors(count_factors)

        iterations = 0
        last_err = sys.maxsize
        finished = False
        while not finished:
            iteration_err = self.als(self.ratings)
            finished = self.is_finished(iterations, iteration_err, last_err)
            last_err = iteration_err
            iterations += 1

        print("finished factor {} on iterations={} rmse={}".format(count_factors, iterations, iteration_err))
        # ratings_train = round(self.user_factors.dot(self.item_factors.T), 5).toarray()
        return self.user_factors, self.item_factors

    # factors - массив [15, 20, 30, 40, 50, 75, 100]
    def train_test(self, step_save=1, factors=None, data_train=None, data_test=None):
        errors = []
        for current_factors in factors:
            errors_factor = []
            print("Training model with {} factors".format(current_factors))

            self.initialize_factors(current_factors)

            iterations = 0
            last_train_mse = sys.maxsize
            last_test_mse = sys.maxsize
            finished = False
            while not finished:
                train_mse = self.als(data_train)
                test_mse = self.rmse(data_test)
                finished = self.is_finished(iterations, last_train_mse, train_mse, last_test_mse, test_mse)
                last_train_mse = train_mse
                last_test_mse = test_mse
                iterations += 1
                # print("finished factor {} on iterations={} train_mse={} test_mse={}".format(current_factors,
                #                                                                             iterations,
                #                                                                             train_mse, test_mse))
                if (iterations % step_save == 0 or iterations==1):
                    errors_factor.append((iterations, np.round(train_mse, 5), np.round(test_mse, 5)))
            errors.append(errors_factor)
        return errors

    def als(self, ratings):
        user_size, item_size = ratings.shape

        uTu = self.user_factors.T.dot(self.user_factors)
        for j in range(item_size):
            j_row = ratings[:, j].T.toarray()
            self.item_factors[j] = spsolve(uTu + self.lI, self.user_factors.T.dot(j_row.T))

        vTv = self.item_factors.T.dot(self.item_factors)
        for i in range(user_size):
            i_row = ratings[i, :].toarray()
            self.user_factors[i] = spsolve(vTv + self.lI, self.item_factors.T.dot(i_row.T))
        return self.rmse(ratings)

    def rmse(self, ratings):
        W = (ratings > 0.).astype(np.float32)
        count_ratings = W.sum()
        return math.sqrt(((W.toarray() * (
                    ratings - self.user_factors.dot(self.item_factors.T)).toarray()) ** 2).sum() / count_ratings)

    def is_finished(self, current_iteration, current_err, last_err, last_test_mse=0.0, test_mse=0.0):
        return current_iteration >= self.MAX_ITERATIONS or abs(last_err - current_err) < 0.001
