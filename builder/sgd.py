import sys
import math
import random
import numpy as np
import pandas as pd
from decimal import Decimal
import scipy.sparse as sparse
from collections import defaultdict
from scipy.sparse.linalg import spsolve


class SGD(object):

    def __init__(self, ratings=None, max_iterations=100, regularization=0.01, rate=0.1):
        self.ratings = ratings
        self.MAX_ITERATIONS = max_iterations
        self.regularization = Decimal(regularization)
        self.rate = Decimal(rate)

        self.count_ratings = ratings.query("rating != 0").shape[0]
        self.all_ratings_mean = Decimal(ratings['rating'].sum() / self.count_ratings)

        self.item_bias = None
        self.user_bias = None
        self.user_factors = None
        self.item_factors = None

    def initialize_factors(self, count_factors=25):
        user_size = len(list(np.sort(self.ratings.user_id.unique())))
        item_size = len(list(np.sort(self.ratings.book_id.unique())))

        self.item_factors = np.full((item_size, count_factors), Decimal(0.05))
        self.user_factors = np.full((user_size, count_factors), Decimal(0.1))

        self.user_bias = defaultdict(lambda: 0)
        self.item_bias = defaultdict(lambda: 0)

    def train(self, count_factors=2):
        self.initialize_factors(count_factors)
        ratings = self.ratings.values

        index_randomized = random.sample(range(0, int(self.count_ratings)), (int(self.count_ratings)))
        iterations = 0
        last_err = sys.maxsize
        iteration_err = sys.maxsize
        finished = False

        while not finished:
            iteration_err = self.stocastic_gradient_descent(count_factors, index_randomized, ratings)
            iterations += 1
            finished = self.finished(iterations, last_err, iteration_err)
            last_err = iteration_err

        print("finished factor {} on iterations={} rmse={}".format(count_factors, iterations, iteration_err))
        # return self.train_matrix()
        return self.user_factors, self.item_factors, self.user_bias, self.item_bias

    # factors - массив [15, 20, 30, 40, 50, 75, 100]
    def train_test(self, step_save=1, factors=None, data_train=None, data_test=None):
        errors = []
        train = data_train.values
        test = data_test.values

        self.count_ratings = len(train)
        self.all_ratings_mean = Decimal(np.asarray(train).sum(axis=0)[2] / self.count_ratings)

        for current_factors in factors:
            errors_factor = []
            print("Training model with {} factors".format(current_factors))

            self.initialize_factors(current_factors)

            iterations = 0
            index_randomized = random.sample(range(0, len(train)), (len(train)))
            last_train_mse = sys.maxsize
            last_test_mse = sys.maxsize
            finished = False

            while not finished:
                train_mse = self.stocastic_gradient_descent(current_factors, index_randomized, train)
                iterations += 1
                test_mse = self.calculate_rmse(test, current_factors)

                finished = self.finished(iterations, last_train_mse, train_mse)
                last_train_mse = train_mse
                last_test_mse = test_mse

                # print("finished factor {} on iterations={} train_mse={} test_mse={}".format(current_factors, iterations, train_mse, test_mse))

                if (iterations % step_save == 0 or iterations==1):
                    errors_factor.append((iterations, np.round(train_mse, 5), np.round(test_mse, 5)))

            errors.append(errors_factor)
        return errors

    def stocastic_gradient_descent(self, count_factors, index_randomized, ratings):
        rmse_current = 0
        for inx in index_randomized:
            row = ratings[inx]
            u = int(row[0])
            i = int(row[1])
            rating = Decimal(row[2])
            if (rating != 0.0):
                predict = self.predict(u, i)
                err = rating - predict
                rmse_current += err * err

                self.user_bias[u] += self.rate * (err - self.regularization * self.user_bias[u])
                self.item_bias[i] += self.rate * (err - self.regularization * self.item_bias[i])

                for j in range(count_factors):
                    self.user_factors[u][j] += self.rate * (
                                err * self.item_factors[i][j] - self.regularization * self.user_factors[u][j])
                    self.item_factors[i][j] += self.rate * (
                                err * self.user_factors[u][j] - self.regularization * self.item_factors[i][j])

        rmse_end_all = self.calculate_rmse(ratings, count_factors)
        # rmse_current = math.sqrt(rmse_current / self.count_ratings)
        return rmse_end_all

    def predict(self, user, item):
        uv = np.dot(self.item_factors[item], self.user_factors[user].T)
        b_ui = self.all_ratings_mean + self.user_bias[user] + self.item_bias[item]
        prediction = b_ui + uv
        return prediction

    def calculate_rmse(self, ratings, factor):
        def difference(row):
            rating = Decimal(row[2])
            if (rating != 0.0):
                user = int(row[0])
                item = int(row[1])

                # Произведение uv на заполненных факторах (берем длину текущего фактора)
                uv = np.dot(self.item_factors[item][:factor + 1], self.user_factors[user][:factor + 1].T)
                b_ui = self.all_ratings_mean + self.user_bias[user] + self.item_bias[item]
                prediction = b_ui + uv

                MSE = (rating - prediction) ** 2
                return MSE
            return 0

        squared = np.apply_along_axis(difference, 1, ratings).sum()
        return math.sqrt(squared / len(ratings))

    def finished(self, iterations, last_err, current_err, last_test_mse=0.0, test_mse=0.0):
        if last_test_mse < test_mse or iterations >= self.MAX_ITERATIONS or abs(last_err - current_err) < 0.001:
            return True
        else:
            return False

    def train_matrix(self):
        users_size = len(self.user_factors)
        items_size = len(self.item_factors)
        result = np.ndarray(shape=(users_size, items_size), dtype=float, order='F')
        for u in range(users_size):
            for v in range(items_size):
                result[u][v] = float(round(
                    round(self.all_ratings_mean, 5) + self.user_bias[u] + self.item_bias[v] + np.dot(
                        self.user_factors[u],
                        self.item_factors[v]),
                    5))
        return result