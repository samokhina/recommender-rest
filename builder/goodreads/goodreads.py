import json
import requests
from xmljson import parker
from xml.etree.ElementTree import fromstring
from json import dumps


class GoodReads(object):
    key = "zGsFwkC2OYSH8TiMofNg"

    get_book_url = "https://www.goodreads.com/book/show/"

    def get_books_by_id(self, ids):

        recommendations = []
        for id in ids:
            response = requests.get(self.get_book_url + str(id) + ".xml?key=" + self.key)
            result = dumps(parker.data(fromstring(response.text)))
            book = json.loads(result)["book"]
            authors = []
            if (isinstance(book["authors"]["author"], list)):
                for author in book["authors"]["author"]:
                    authors.append({
                        "id": author["id"],
                        "name": author["name"],
                        "averageRating": author["average_rating"],
                        "imageUrl": author["image_url"],
                        "url": author["link"],
                        "ratingsCount": author["ratings_count"],
                    })
            else:
                author = book["authors"]["author"]
                authors.append({
                    "id": author["id"],
                    "name": author["name"],
                    "averageRating": author["average_rating"],
                    "imageUrl": author["image_url"],
                    "url": author["link"],
                    "ratingsCount": author["ratings_count"],
                })

            book_data = {
                "id": book["id"],
                "authors": authors,
                "title": book["title"],
                "description": book["description"],
                "url": book["url"],
                "imageUrl": book["image_url"],
                "averageRating": book["average_rating"],
            }
            recommendations.append(book_data)
        return recommendations
