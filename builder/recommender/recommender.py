import MySQLdb
import numpy as np
import pandas as pd
import scipy.sparse as sparse
from builder.reviews.reviews import Reviews


class Recommender(object):

    def get_count_recommendations_for_user_ALS(self,
                                               userId=None,
                                               indexes=None,
                                               user_factors=None,
                                               items_factors=None,
                                               count_recommendations=5):
        if userId:
            items_result = \
                sparse.csr_matrix(user_factors).dot(sparse.csr_matrix(items_factors).T).toarray()[0]

            return self.get_recommendations(
                userId=userId,
                indexes=indexes,
                items_result=items_result,
                count_recommendations=count_recommendations)
        else:
            print('Ошибка: Пользователь не указан')

    def get_count_recommendations_for_user_SGD(self,
                                               userId=None,
                                               indexes=None,
                                               user_factors=None,
                                               items_factors=None,
                                               user_bias=None,
                                               items_bias=None,
                                               all_ratings_mean=None,
                                               count_recommendations=5):
        if userId:
            items_size = len(items_factors)
            items_result = [0] * items_size
            for v in range(items_size):
                items_result[v] = float(round(
                    round(all_ratings_mean, 5) + user_bias + items_bias[v] + np.dot(user_factors, items_factors[v]), 5))

            return self.get_recommendations(
                userId=userId,
                indexes=indexes,
                items_result=items_result,
                count_recommendations=count_recommendations)
        else:
            print('Ошибка: Пользователь не указан')

    def get_recommendations(self,
                            userId=None,
                            indexes=None,
                            items_result=None,
                            count_recommendations=None):

        connection = MySQLdb.connect(
            host="localhost",
            user="irina",
            passwd="1357",
            db="recommendations")
        reviews_data = pd.read_sql("SELECT * FROM train_review LIMIT 1000", connection)
        reviews = Reviews(reviews_data)
        users_ids, items_ids, data_matrix, data = reviews.transformation_reviews()
        connection.close()

        print('len: ', len(items_result))
        result = [0] * len(items_result)
        for index, value in enumerate(indexes):
            id = int(items_ids.query("bookId=='" + str(value) + "'")['book_id'].values[0])
            result[id] = round(items_result[index], 5)
        result = np.array(result)

        index_user_in_train = np.int(users_ids.query("userId=='" + str(userId) + "'")['user_id'].values[0])

        item_indices = np.arange(result.shape[0])
        rated_mask = data_matrix.toarray()[index_user_in_train, :] != 0.

        result = result[~rated_mask]
        item_indices = item_indices[~rated_mask]  # индексы неоцененных

        order = np.argsort(-result)[:int(count_recommendations)]
        items = item_indices[order]  # индексы неоцененных в порядке убывания оценок

        items_result_titles = []
        for i in range(len(items)):
            items_result_titles.append(
                items_ids.query("book_id=='" + str(items[i]) + "'")['bookId'].values[0])

        return items_result_titles, result[order]
