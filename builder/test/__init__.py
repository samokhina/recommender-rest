from threading import Thread
import time
import sys
sys.path.append("..")
import MySQLdb
import pandas as pd
import numpy.linalg as linalg
from als import ALS
from sgd import SGD
from reviews.reviews import Reviews

sql_insert_test = "INSERT INTO train_test(`regularization`, `rate`, `maxIterations`, `dividingSample`, `method_id`) VALUES ("
sql_insert_factor = "INSERT INTO train_testfactor(`factor`, `test_id`) VALUES ("
sql_insert_error = "INSERT INTO train_error(`iteration`, `errorTrain`, `errorTest`, `factor_id`) VALUES ("


def run_test_sgd(data=None,
                 data_train=None,
                 data_test=None,
                 factors=None,
                 step_save=None,
                 rate=None,
                 regularization=None,
                 max_iterations=None,
                 dividing_sample=None,
                 method_id=None):
    sgd_test = SGD(ratings=data,
                        max_iterations=max_iterations,
                        regularization=regularization,
                        rate=rate)

    cursor.execute(sql_insert_test + str(regularization) + "," + str(rate) + "," + str(
        max_iterations) + ",'" + dividing_sample + "'," + str(method_id) + ");")
    connection.commit()
    id_test = cursor.lastrowid

    print('sgd run')
    start_time = time.time()

    errors = sgd_test.train_test(step_save=step_save,
                                 data_train=data_train,
                                 data_test=data_test,
                                 factors=factors)
    end_time = time.time()

    print('errors: ', errors[0])

    print('start_time: ', start_time)
    print('end_time: ', end_time)
    print("--- %s seconds ---" % (end_time - start_time))

    current_factor = 0
    for factor in factors:
        cursor.execute(sql_insert_factor + str(factor) + "," + str(id_test) + ");")
        connection.commit()
        id_factor = cursor.lastrowid

        for error in errors[current_factor]:
            cursor.execute(sql_insert_error + str(error[0]) + "," + str(error[1]) + "," + str(error[2]) + "," + str(
                id_factor) + ");")
            connection.commit()
        current_factor += 1


def run_test_als(data=None,
                 data_train=None,
                 data_test=None,
                 factors=None,
                 step_save=None,
                 rate=None,
                 regularization=None,
                 max_iterations=None,
                 dividing_sample=None,
                 method_id=None):
    als_test = ALS(ratings=data,
                        max_iterations=max_iterations,
                        regularization=regularization,
                        rate=rate)

    cursor.execute(sql_insert_test + str(regularization) + "," + str(rate) + "," + str(
        max_iterations) + ",'" + dividing_sample + "'," + str(method_id) + ");")
    connection.commit()
    id_test = cursor.lastrowid

    print('als run')
    start_time = time.time()

    errors = als_test.train_test(step_save=step_save,
                                 data_train=data_train,
                                 data_test=data_test,
                                 factors=factors)
    end_time = time.time()

    print('errors: ', errors[0])

    print('start_time: ', start_time)
    print('end_time: ', end_time)
    print("--- %s seconds ---" % (end_time - start_time))

    current_factor = 0
    for factor in factors:
        cursor.execute(sql_insert_factor + str(factor) + "," + str(id_test) + ");")
        connection.commit()
        id_factor = cursor.lastrowid

        for error in errors[current_factor]:
            cursor.execute(sql_insert_error + str(error[0]) + "," + str(error[1]) + "," + str(error[2]) + "," + str(
                id_factor) + ");")
            connection.commit()
        current_factor += 1


if __name__ == '__main__':
    connection = MySQLdb.connect(
        host="localhost",
        user="irina",
        passwd="1357",
        db="recommendations")

    # reviews_data = pd.read_sql("SELECT * FROM review", connection)
    reviews_data = pd.read_sql("SELECT * FROM train_review LIMIT 50000", connection)
    reviews = Reviews(reviews_data)
    users_ids, books_ids, data_matrix, data = reviews.transformation_reviews()
    data_train_matrix, data_train, data_test_matrix, data_test = reviews.split_reviews(percent_test=20)
    cursor = connection.cursor()

    print('Finished split data')


    thread1 = Thread(target=run_test_sgd, args=(data,
                                                data_train,
                                                data_test,
                                                [20],
                                                2,
                                                0.02,
                                                0.05,
                                                100,
                                                '20,80',
                                                1
                                                ))

    thread2 = Thread(target=run_test_als, args=(data_matrix,
                                                data_train_matrix,
                                                data_test_matrix,
                                                [10],
                                                1,
                                                0.001,
                                                0.2,
                                                100,
                                                '20,80',
                                                2))

    # thread1.start()
    thread2.start()
    # thread1.join()
    thread2.join()

    run_test_sgd(data=data,
                 data_train=data_train,
                 data_test=data_test,
                 factors=[3, 5, 7, 9, 11, 13, 15, 17, 19, 21],
                 step_save=3,
                 rate=0.02,
                 regularization=0.5,
                 max_iterations=100,
                 dividing_sample='20,80',
                 method_id=1)

    run_test_als(data=data_matrix,
                 data_train=data_train_matrix,
                 data_test=data_test_matrix,
                 factors=[5, 7, 8, 10, 15],
                 step_save=1,
                 rate=0.1,
                 regularization=0.5,
                 max_iterations=50,
                 dividing_sample='20,80',
                 method_id=2)

    connection.close()
