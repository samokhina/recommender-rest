import MySQLdb
import numpy as np
import pandas as pd
import sys

sys.path.append("..")
from builder.als import ALS
from builder.sgd import SGD
from reviews.reviews import Reviews


def get_recommendations_for_user_ALS(user=None,
                                     ratings=None,
                                     users_ids=None,
                                     items_ids=None,
                                     users_factors=None,
                                     items_factors=None,
                                     count_recommendations=5):
    if user:
        index_user_in_train = np.int(users_ids.query("userId=='" + str(user) + "'")['user_id'].values[0])

        # Получение результирующего вектора items_result по всем товарам для index_user_in_train
        items_result = round(users_factors[index_user_in_train].dot(items_factors.T), 5).toarray()[0]

        return get_recommendations(
            ratings=ratings,
            items_ids=items_ids,
            items_result=items_result,
            index_user_in_train=index_user_in_train,
            count_recommendations=count_recommendations)
    else:
        print('Ошибка: Пользователь не указан')


def get_recommendations_for_user_SGD(user=None,
                                     ratings=None,
                                     users_ids=None,
                                     items_ids=None,
                                     users_factors=None,
                                     items_factors=None,
                                     user_bias=None,
                                     item_bias=None,
                                     all_ratings_mean=None,
                                     count_recommendations=5):
    if user:
        index_user_in_train = np.int(users_ids.query("userId=='" + str(user) + "'")['user_id'].values[0])

        # Получение результирующего вектора items_result по всем товарам для index_user_in_train
        items_size = len(items_factors)
        items_result = np.ndarray(shape=(1, items_size), dtype=float, order='F')
        for v in range(items_size):
            items_result[index_user_in_train][v] = float(round(
                round(all_ratings_mean, 5) + user_bias[index_user_in_train] + item_bias[v] + np.dot(
                    users_factors[index_user_in_train],
                    items_factors[v]),
                5))
        items_result = items_result[0]

        return get_recommendations(ratings=ratings,
                                   items_ids=items_ids,
                                   items_result=items_result,
                                   index_user_in_train=index_user_in_train,
                                   count_recommendations=count_recommendations)
    else:
        print('Ошибка: Пользователь не указан')


def get_recommendations(ratings=None,
                        items_ids=None,
                        items_result=None,
                        index_user_in_train=None,
                        count_recommendations=None):
    item_indices = np.arange(items_result.shape[0])
    rated_mask = ratings.toarray()[index_user_in_train, :] != 0.

    items_result = items_result[~rated_mask]
    item_indices = item_indices[~rated_mask]  # индексы неоцененных

    order = np.argsort(-items_result)[:count_recommendations]
    items = item_indices[order]  # индексы неоцененных в порядке убывания оценок

    items_result_titles = []
    for i in range(len(items)):
        items_result_titles.append(
            items_ids.query("book_id=='" + str(items[i]) + "'")['bookId'].values[0])
    print("Top", count_recommendations, "recommended books are: %r" % items_result_titles)
    return items, items_result[order]


sql_insert_train = "INSERT INTO recommender_train(`regularization`, `rate`, `maxIterations`, `factorsCount`, `ratingMean`, `method_id`) VALUES ("
sql_insert_userfactors = "INSERT INTO recommender_userfactors(`userId`, `train_id`) VALUES ("
sql_insert_userfactor = "INSERT INTO recommender_userfactor(`factor`, `value`, `userFactors_id`) VALUES ("
sql_insert_itemfactors = "INSERT INTO recommender_itemfactors(`itemId`, `train_id`) VALUES ("
sql_insert_itemfactor = "INSERT INTO recommender_itemfactor(`factor`, `value`, `itemFactors_id`) VALUES ("
sql_insert_userbias = "INSERT INTO recommender_userbias(`userId`, `factor`, `value`, `train_id`) VALUES ("
sql_insert_itembias = "INSERT INTO recommender_itembias(`itemId`, `factor`, `value`, `train_id`) VALUES ("


def run_train_als(data=None,
                  users_ids=None,
                  items_ids=None,
                  max_iterations=None,
                  regularization=None,
                  rate=None,
                  count_factors=None,
                  method_id=None):
    als = ALS(ratings=data,
              max_iterations=max_iterations,
              regularization=regularization,
              rate=rate)

    rating_mean = np.round(data.sum() / (data > 0.).astype(np.float32).sum(), 5)
    cursor.execute(sql_insert_train + str(regularization) + "," + str(rate) + "," + str(
        max_iterations) + "," + str(count_factors) + "," + str(rating_mean) + "," + str(method_id) + ");")
    connection.commit()
    train_id = cursor.lastrowid

    users_factors, items_factors = als.train(count_factors=count_factors)

    for user in users_ids.values:
        user_id = user[1]
        user_factors = users_factors[int(user[0])].toarray()[0]
        cursor.execute(sql_insert_userfactors + str(user_id) + "," + str(train_id) + ");")
        connection.commit()
        userFactors_id = cursor.lastrowid
        for factor, value in enumerate(user_factors):
            cursor.execute(
                sql_insert_userfactor + str(factor + 1) + "," + str(round(value, 5)) + "," + str(userFactors_id) + ");")
            connection.commit()

    for item in items_ids.values:
        item_id = item[1]
        item_factors = items_factors[int(item[0])].toarray()[0]
        cursor.execute(sql_insert_itemfactors + str(item_id) + "," + str(train_id) + ");")
        connection.commit()
        itemFactors_id = cursor.lastrowid
        for factor, value in enumerate(item_factors):
            cursor.execute(
                sql_insert_itemfactor + str(factor + 1) + "," + str(round(value, 5)) + "," + str(itemFactors_id) + ");")
            connection.commit()


def run_train_sgd(data=None,
                  users_ids=None,
                  items_ids=None,
                  max_iterations=None,
                  regularization=None,
                  rate=None,
                  count_factors=None,
                  method_id=None):
    sgd = SGD(ratings=data,
              max_iterations=max_iterations,
              regularization=regularization,
              rate=rate)

    rating_mean = data['rating'].sum() / data.query("rating != 0").shape[0]

    cursor.execute(sql_insert_train + str(regularization) + "," + str(rate) + "," + str(
        max_iterations) + "," + str(count_factors) + "," + str(rating_mean) + "," + str(method_id) + ");")
    connection.commit()
    train_id = cursor.lastrowid

    users_factors, items_factors, user_bias, item_bias = sgd.train(count_factors=count_factors)

    for index, user in enumerate(users_ids.values):
        user_id = user[1]
        user_factors = users_factors[int(user[0])]

        cursor.execute(
            sql_insert_userbias + str(user_id) + "," + str(index + 1) + "," + str(user_bias[int(user[0])]) + "," + str(
                train_id) + ");")
        connection.commit()

        cursor.execute(sql_insert_userfactors + str(user_id) + "," + str(train_id) + ");")
        connection.commit()
        userFactors_id = cursor.lastrowid
        for factor, value in enumerate(user_factors):
            cursor.execute(
                sql_insert_userfactor + str(factor + 1) + "," + str(round(value, 5)) + "," + str(userFactors_id) + ");")
            connection.commit()

    for index, item in enumerate(items_ids.values):
        item_id = item[1]
        item_factors = items_factors[int(item[0])]

        cursor.execute(
            sql_insert_itembias + str(item_id) + "," + str(index + 1) + "," + str(item_bias[int(item[0])]) + "," + str(
                train_id) + ");")
        connection.commit()

        cursor.execute(sql_insert_itemfactors + str(item_id) + "," + str(train_id) + ");")
        connection.commit()
        itemFactors_id = cursor.lastrowid
        for factor, value in enumerate(item_factors):
            cursor.execute(
                sql_insert_itemfactor + str(factor + 1) + "," + str(round(value, 5)) + "," + str(itemFactors_id) + ");")
            connection.commit()


if __name__ == '__main__':
    connection = MySQLdb.connect(
        host="localhost",
        user="irina",
        passwd="1357",
        db="recommendations")

    reviews_data = pd.read_sql("SELECT * FROM train_review LIMIT 1000", connection)
    reviews = Reviews(reviews_data)
    users_ids, items_ids, data_matrix, data = reviews.transformation_reviews()
    cursor = connection.cursor()

    # run_train_als(data=data_matrix,
    #               users_ids=users_ids,
    #               items_ids=items_ids,
    #               max_iterations=100,
    #               regularization=0.05,
    #               rate=0.02,
    #               count_factors=70,
    #               method_id=2)

    run_train_sgd(data=data,
                  users_ids=users_ids,
                  items_ids=items_ids,
                  max_iterations=100,
                  regularization=0.2,
                  rate=0.02,
                  count_factors=20,
                  method_id=1)

    # TODO Ниже код для проверки

    # # TODO SGD
    # sgd = SGD(ratings=data,
    #                max_iterations=25,
    #                regularization=0.05,
    #                rate=0.1)
    # users_factors, items_factors, user_bias, item_bias, all_ratings_mean = sgd.train(count_factors=5)
    # userId = 1
    # items_sgd, ratings_sgd = get_recommendations_for_user_SGD(
    #     user=userId,
    #     ratings=data_matrix,
    #     users_ids=users_ids,
    #     items_ids=items_ids,
    #     users_factors=users_factors,
    #     items_factors=items_factors,
    #     user_bias=user_bias,
    #     item_bias=item_bias,
    #     all_ratings_mean=all_ratings_mean,
    #     count_recommendations=5
    # )

    # # TODO ALS
    # als = ALS(ratings=data_matrix,
    #                max_iterations=25,
    #                regularization=0.01,
    #                rate=0.01)
    # users_factors, items_factors = als.train(count_factors=5)
    # userId = 1
    # items_als, ratings_als = get_recommendations_for_user_ALS(
    #     user=userId,
    #     ratings=data_matrix,
    #     users_ids=users_ids,
    #     items_ids=items_ids,
    #     users_factors=users_factors,
    #     items_factors=items_factors,
    #     count_recommendations=5
    # )

    connection.close()
